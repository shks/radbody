//
//  ofxGridDraw.h
//
//  Created by Shunichi Kasahara on 2015/06/23.
//
//

#ifndef kinectExample_ofxGridDraw_h
#define kinectExample_ofxGridDraw_h
#include "ofMain.h"

class ofxGridDraw
{
public:
    
    static void drawGrid(float res_x, float res_y)
    {
        float w = ofGetWidth();
        float h = ofGetHeight();
        
        for(int i = 0; i < h; i+=res_y)
        {
            ofLine(0,i,w,i);
        }
        
        for(int j = 0; j < w; j+=res_x)
        {
            ofLine(j,0,j,h);
        }
    }
    

    static void drawGridFloor( ofPoint centerPos, ofVec3f size, ofVec3f res, ofColor lineCol= ofColor(255, 100), ofColor fillCol= ofColor(30, 0, 250, 20))
    {
        
        ofPushStyle();
        ofEnableBlendMode(OF_BLENDMODE_ADD);
        ofPushMatrix();
        
        ofTranslate(centerPos);
        
        ///fillCol
        {
            ofFill();
            ofSetColor(fillCol);
            ofPushMatrix();
            ofScale(size.x, 0.1, size.z);
            ofDrawBox(1.0);
            ofPopMatrix();
        }
        ofDrawAxis(50.0);
        
        ofTranslate( -1. *  ofPoint( size.x/2, 0, size.y/2));
        
        
        ofNoFill();
        ofSetLineWidth(2.0);
        ofSetColor(lineCol);

        
        for(int i = 0; i <= size.z; i+=res.z)
        {
            ofLine(0, 0, i, size.x, 0, i);
        }
        
        for(int j = 0; j <= size.x; j+=res.x)
        {
            ofLine(j, 0, 0,j,0,size.x);
        }
        
        
        ofPopMatrix();
        ofPopStyle();
        
        
        
    }
    
    
    static void drawGridBox(ofPoint pos, ofVec3f size, ofVec3f res)
    {
        
        ofPushStyle();
        
        ofSetLineWidth(4.0);
        
        ofEnableBlendMode(OF_BLENDMODE_ADD);
        ofPushMatrix();
        ofTranslate(pos);
//        ofSetColor(255, 255, 255, 40);
        ofSetColor(255, 255, 255, 40);

        
        for(int k = 0 ; k <= size.z ; k += res.z)
        {
            ofPushMatrix();
            ofTranslate(0, 0, k);
            
            for(int i = 0; i <= size.y; i+=res.y)
            {
                ofLine(0,i,size.x,i);
            }
            
            for(int j = 0; j <= size.x; j+=res.x)
            {
                ofLine(j,0,j,size.y);
            }
            
            ofPopMatrix();
        }
        
        
        for(int j = 0; j <= size.x; j+=res.x)
        {
            for(int i = 0; i <= size.y; i+=res.y)
            {
                ofLine(i,j,0,i,j,size.z);
            }
        }
        

        ofSetColor(255, 255, 255, 120);
        ofNoFill();
        
        ofPoint cen = size / 2.0;
        ofDrawBox(cen.x, cen.y, cen.z, size.x, size.y, size.z);

        ofPushMatrix();
        ofTranslate(cen);
        ofFill();
        ofSetColor(0, 24, 123, 30);
        ofRect( -0.5 * size.x, -0.5 * size.y, size.x, size.y);

        ofPopMatrix();
        
        ofPopMatrix();
        
        ofSetColor(255);
        ofDrawBitmapString( ofToString(pos) , pos);
        ofDrawBitmapString( ofToString(pos + ofPoint(size.x, 0,0)) , pos + ofPoint(size.x, 0,0));
        ofDrawBitmapString( ofToString(pos + ofPoint(size.x, size.y,0)) , pos + ofPoint(size.x, size.y,0));
        ofDrawBitmapString( ofToString(pos + ofPoint(size.x, size.y, size.z)) , pos + ofPoint(size.x, size.y, size.z));
        ofDrawBitmapString( ofToString(pos + ofPoint(0, size.y, size.z)) , pos + ofPoint(0, size.y, size.z));
        ofDrawBitmapString( ofToString(pos + ofPoint(0, 0, size.z)) , pos + ofPoint(0, 0, size.z));
        
        ofPopStyle();
        
    }
    
    
    
    
};

#endif
