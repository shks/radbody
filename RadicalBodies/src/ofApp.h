#pragma once

#include "ofMain.h"
#include "ofxSquashBuddies.h"
#include "ofxOculusDK2.h"
#include "ofxTurboJpeg.h"
#include "ofxMultiKinectV2.h"
#include "ofxGui.h"
#include "ofxGridDraw.h"

#include "ofxOSCcommunicator.h"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
    void updateMesh();
    
    // - - -  OSC communicator - - - - //
    bool                    mIsDoDebug;
    
    // - - -  OSC communicator - - - - //
    ofxOSCcommunicator      mOSCcommunicator;
    void onReceiveOSC(ofxOscMessage & prt);

    
    // - - - - -draw functions - - - - - - - //
    void drawKinectSensors();
    void drawScene();
    void drawVertexes();

    shared_ptr<ofShader>	mShdInstanced;
    bool                    isShaderDirty;
    ofImage                 textureImg;
    string                  vertShaderFileName;
    string                  fragShaderFileName;

    
    //- - - - - ofxSquashBuddies - - - - - //
    ofxSquashBuddies::Sender    sender;
    
    const static int            NUM_BUDDIES = 3;
    ofxSquashBuddies::Receiver  receivers[NUM_BUDDIES];
    ofMesh                      receivedMeshs[NUM_BUDDIES];
    
    void setupSquash();
    void updateSquash();
  
    ofVboMesh mesh;

//    ofVboMesh mesh;
    //ofMesh mesh;
    
    // - - - - - - ofxOculusDK2 - - - - - //
    ofxOculusDK2		oculusRift;
    ofEasyCam           ecam;
    ofEasyCam           gcam;

    void                setupCameras();
    void                drawOVR();
    
    // - - - - - - ofxMultiKinectV2 - - - - - //
    ofxMultiKinectV2    kinect0;
    ofShader            depthShader;
    ofTexture           depthTex;
    void openKinect();
    void updateKinect();
    
    
    // - - - - - Sound input - - - - - //
    ofSoundStream soundStream;
    void setupAudio();
    void audioIn(float * input, int bufferSize, int nChannels);
    void updateAudio();
    void drawAudio();
    
    vector <float> left;
    vector <float> right;
    vector <float> volHistory;
    
    int 	bufferCounter;
    int 	drawCounter;
    
    float smoothedVol;
    float scaledVol;
    
    
    
    
    // - - - - - GUI - - - //
    ofxPanel            gui;
    ofParameterGroup    parameters;
    void                setupGui();

    ofParameter<bool>   mDoDebug;
    
    
    ofParameter<bool>   mKinectDebugInfor;
    ofParameter<bool>   mDoShader;

    
    ofParameter<bool>   mKinectEnable;
    ofParameter<int>    mKinectReadResolusion;
    
    ofParameter<int>    mPointsNumber;
    
    ofParameter<ofVec2f> mMinRangeDepth;
    ofParameter<ofVec2f> mMaxRangeDepth;
    ofParameter<float>  mMinDepth;
    ofParameter<float>  mMaxDepth;
    
    
    
    
    ofParameter<bool>   mDK2Enable;

    ofParameter<ofVec3f> mKinectSpaceCalibTrans;
    ofParameter<ofVec3f> mKinectSpaceCalibRot;
    ofParameter<ofVec3f> mKinectSpaceRangesCenter;
    
    
    //--camera space
    ofParameter<bool>   mOutSideView;

    //
    float                   testAnimation;
    ofParameter<ofVec3f>    effectCenter;
    ofParameter<int>        hitFinderChannel;
    ofParameter<float>      effectAmp;
    
    ofParameter<bool>       mVirtualMirror;
    ofParameter<float>      virtualMirroroffset;

    ofParameter<bool>       mVirtuaFloor;
    ofParameter<float>       mVirtuaFlooroffset;
    
    
    ofParameter<bool>       mSendMesh;
    ofParameter<bool>       mRevMesh;
    
    ofParameter<bool>       mReceivedMeshDraw;
    ofParameter<float>      mReceivedMeshNum;
    ofParameter<ofVec3f>    mReceiveMeshOffset;
    // -
    
    ofxPanel                effect_gui;
    ofParameterGroup        effect_parameters;
    ofParameter<int>        copyHuman;
    ofParameter<ofVec3f>    copyHumanIntervel;
    
    
    ofParameter<float>        mSignalBang;
    
    
    //[y][x]
    const static int DEPTH_IMAGE_W = 512;
    const static int DEPTH_IMAGE_H = 424;

    float                   mZMapSmoothed[DEPTH_IMAGE_H][DEPTH_IMAGE_W];
    float                   mZMap[DEPTH_IMAGE_H][DEPTH_IMAGE_W];
    ofParameter<float>      mFilter_ZTDif_Thresh_Time;
    ofParameter<float>      mFilter_ZTDif_Thresh_Space;
    //h = 424
    //w = 512
    
    ofParameter<float>      mSoundBufferDivRes;
    
    
    
    
};
