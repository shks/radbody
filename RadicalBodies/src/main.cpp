#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
    ofGLWindowSettings settings;
    settings.width = 1920;
    settings.height = 1080;
    settings.setGLVersion(4,0); /// < select your GL Version here
    ofCreateWindow(settings); ///< create your window here
    
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(new ofApp());


}
