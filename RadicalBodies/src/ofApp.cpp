#include "ofApp.h"
#include "ofxTimeMeasurements.h"
#include "ofxSuperLog.h"

#include "ofxGridDraw.h"
#define STRINGIFY(x) #x


//--------------------------------------------------------------
void ofApp::setup(){
    
    TIME_SAMPLE_SET_FRAMERATE( 60.0f ); //set the app's target framerate (MANDATORY)
    TIME_SAMPLE_SET_DRAW_LOCATION( TIME_MEASUREMENTS_BOTTOM_RIGHT ); //specify a drawing location (OPTIONAL)
    TIME_SAMPLE_SET_AVERAGE_RATE(0.1);	//averaging samples, (0..1],1.0 gets you no averaging at all

    bool logToConsole = true;
    bool logToScreen = true;

//    ofSetLoggerChannel(ofxSuperLog::getLogger(logToConsole, logToScreen, "logs"));
//    ofxSuperLog::getLogger()->setMaximized(true);
    
	auto port = 4444;
	string ipAddress = "127.0.0.1";

	auto result = ofSystemTextBoxDialog("Target IP Address (Default : " + ipAddress + ")");
	if (!result.empty()) {
		ipAddress = result;
	}
    //1ipAddress = "192.168.1.101";
    
	this->sender.init(ipAddress, port);

	ofSetWindowTitle("Sending to : " + ipAddress + ":" + ofToString(port));
	ofSetFrameRate(75);
    
    mesh.setUsage(GL_DYNAMIC_DRAW);
    mesh.setMode(OF_PRIMITIVE_POINTS);
    
    openKinect();
    setupSquash();
    setupCameras();
    setupGui();
    
    
    isShaderDirty = true;
    //
    testAnimation = 1.0;
    
    mOSCcommunicator.init(NULL, NULL, 3000, 3001);
    ofAddListener(mOSCcommunicator.newReceivedOSCEvent,this,&ofApp::onReceiveOSC);

    vertShaderFileName = "Shaders_GL3/simpleAmp.vert";
    fragShaderFileName = "Shaders_GL3/simpleAmp.frag";

    
    setupAudio();
    
    mIsDoDebug = true;

}

void ofApp::setupGui(){
    
    parameters.setName("MainControl");
    
    // - - - - VIEW CONTROL - - - - //
    parameters.add(mDoDebug.set("Doing Debug", false));
    
    // - - - - VIEW CONTROL - - - - //
    parameters.add(mDK2Enable.set("DK2Enable", false));
    parameters.add(mKinectEnable.set("KinectEnable", false));
    
    
    
    // - - - - Point Cloud - - - - //
    parameters.add(mKinectReadResolusion.set("KinectReadResolusion", 1, 1, 10));
    parameters.add(mPointsNumber.set("PointsNumber", 0, 0, 100000));
    parameters.add(mDoShader.set("DoShader", true));

    parameters.add(mDoShader.set("Calibration", true));

    parameters.add(mFilter_ZTDif_Thresh_Time.set("Filter_ZTDif_Thresh TIME",30,0,120));
    parameters.add(mFilter_ZTDif_Thresh_Space.set("Filter_ZTDif_Thresh SPACE",30,0,120));
    
    
    // - - - - SPACE Calibration - - - - //
    float range = 300.0;
    parameters.add(mKinectSpaceCalibTrans.set("KinectSpaceCalibTrans", ofVec3f(0.0, 0.0, 0.0), ofVec3f(-1.0 * range), ofVec3f(range)));
    parameters.add(mKinectSpaceCalibRot.set("KinectSpaceCalibRot", ofVec3f(0.0, 0.0, 0.0), ofVec3f(0, 0, 0), ofVec3f(360.0, 360.0, 360.0)));
    parameters.add(mKinectSpaceRangesCenter.set("KinectSpaceRangesCenter", ofVec3f(0.0, 0.0, 0.0), ofVec3f(-1.0 * range), ofVec3f(range)));

    
    parameters.add(mMinRangeDepth.set("MinRangeDepth", ofVec2f(0.0), ofVec2f(0.0), ofVec2f(512)));
    parameters.add(mMaxRangeDepth.set("MaxRangeDepth", ofVec2f(0.0), ofVec2f(0.0), ofVec2f(512)));
    parameters.add(mMinDepth.set("MinDepth", 0.0, 0.0, range));
    parameters.add(mMaxDepth.set("MaxDepth", 0.0, 0.0, range));
    
    
    
    parameters.add(mOutSideView.set("OutSideView", false));
    parameters.add(mVirtualMirror.set("mVirtualMirror", false));
    parameters.add(virtualMirroroffset.set("virtualMirroroffset", 100, 0, 500));
    parameters.add(mVirtuaFlooroffset.set("VirtuaFlooroffset", 0,
                                          -500, 500));
    
    parameters.add(mSendMesh.set("Send Mesh", true));
    parameters.add(mRevMesh.set("Receive Mesh", true));
    
    parameters.add(mReceiveMeshOffset.set("Received  MeshOffset", ofVec3f(0.0, 0.0, 0.0), ofVec3f(-1.0 * range * 10), ofVec3f(range * 10)));
    

    parameters.add(mReceivedMeshDraw.set("Received MeshDraw", true));
    parameters.add(mReceivedMeshNum.set("ReceivedMeshNum", 0,0,50000));
    
    gui.setTextColor(ofColor(12,255,120));
    gui.setup(parameters, "mySettings.xml");
    gui.loadFromFile("mySettings.xml");
    gui.setPosition(200, 100);
    // - - - - -
    range = 100.0;
    effect_parameters.setName("Effect Control");
    
    effect_parameters.add(effectCenter.set("effectCenter", ofVec3f(0.0, 0.0, 0.0), ofVec3f(-1.0 * range * 10), ofVec3f(range * 10)));
    effect_parameters.add(hitFinderChannel.set("hitFinderChannel", 0, 0, 10));
    effect_parameters.add(effectAmp.set("effectAmp", 0.1, 0.0, 0.3));

    
    effect_parameters.add(copyHuman.set("copyHuman", 0, 0, 100));
    effect_parameters.add(copyHumanIntervel.set("copyHuman", ofVec3f(100.0, 0.0, 0.0), ofVec3f(-1.0 * range), ofVec3f(300)));

    effect_parameters.add(mSoundBufferDivRes.set("SoundBufferDivRes", 1, 1, 256));
    
    effect_parameters.add(mSignalBang.set("SingalMonitor",0,0.0,1.0));
    
    effect_gui.setDefaultFillColor(ofColor(12,255,120));
    effect_gui.setTextColor(ofColor(12,255,120));
    effect_gui.setName("Effect Control PANEL");
    effect_gui.setup(effect_parameters, "mySettingsEffect.xml");
    effect_gui.setPosition(350, 100);
    
    //mSignalBang
    
}

void ofApp::setupCameras()
{
    
    ecam.setAutoDistance(false);
    ecam.begin();
    ecam.end();
    ecam.setDistance(0);
    ecam.setNearClip(0.01);
    ecam.disableMouseInput();
    

    gcam.setAutoDistance(false);
    gcam.begin();
    gcam.setNearClip(0.001);
    gcam.end();
    gcam.setDistance(3.0);
    
    oculusRift.baseCamera = &ecam;
    oculusRift.setup();
    oculusRift.setUsePredictedOrientation(true);
    oculusRift.lockView = false;
    oculusRift.reloadShader();
    
}

void ofApp::setupSquash()
{
    this->receivers[0].init(4444);
    this->receivers[1].init(4445);
    this->receivers[2].init(4446);
}

void ofApp::setupAudio()
{
    int bufferSize = 256;
    
    
    left.assign(bufferSize, 0.0);
    right.assign(bufferSize, 0.0);
    volHistory.assign(400, 0.0);
    
    bufferCounter	= 0;
    drawCounter		= 0;
    smoothedVol     = 0.0;
    scaledVol		= 0.0;
    
    soundStream.setup(this, 0, 2, 44100, bufferSize, 4);
    
}

void ofApp::openKinect()
{
    kinect0.open(true, true, 0, 2);
    kinect0.start();
    
}
//--------------------------------------------------------------
void ofApp::update(){
    
    mIsDoDebug = mDoDebug.get();
    if(!mIsDoDebug)
    {
        TIME_SAMPLE_DISABLE();
    }else{
        TIME_SAMPLE_ENABLE();
    }

    //Bang indication update
    {
        mSignalBang.set(testAnimation);
    }
    
    TS_START("OSCcommunicator");
    mOSCcommunicator.update();
    TS_STOP("OSCcommunicator");
    
    
    TS_START("Shader runTest");
    if( mDoShader.get() && mIsDoDebug)
    {
        if(ofGetFrameNum() % 60 * 10 == 0)
        {
            isShaderDirty = true;
        }
        
        //test cpde
        if(ofGetFrameNum() % 60 * 20 == 0)
        {
            ofDisableArbTex();
            textureImg.load("particle.png");
        }
    }
    TS_STOP("Shader runTest");
    
    
    TS_START("ShaderDirty");
    
    if (isShaderDirty){
        
        // only reload the shader if it is 'dirty', i.e. the user has either requested reloading
        // or the 'isShaderDirty' flag has been initialized to true in setup()
        
        // Since we are using a shared_ptr around the shader, the old shader will get destroyed
        // automatically as soon as we assign a new shader object to our mShdInstanced.
        
        ofLogNotice() << "Reloading Shader." << vertShaderFileName <<" : " << fragShaderFileName;
        mShdInstanced = shared_ptr<ofShader>(new ofShader());

        mShdInstanced->load(vertShaderFileName, fragShaderFileName);
        //"Shaders_GL3/simpleAmp.vert", "Shaders_GL3/simpleAmp.frag");
        
        
        GLint err = glGetError();
        if (err != GL_NO_ERROR){
            ofLogNotice() << "Load Shader came back with GL error:	" << err;
        }else{
            ofLogNotice() << "Reloading Shader SUCCESS.";
        }

        isShaderDirty = false;
    }
    TS_STOP("ShaderDirty");
    
    
    TS_START("updateAudio");
    updateAudio();
    TS_STOP("updateAudio");
    
    
    TS_START("updateKinect");
    updateKinect();
    TS_STOP("updateKinect");
    
    TS_START("updateSquash:Receive");
    if( mRevMesh.get()){ updateSquash(); }
    TS_STOP("updateSquash:Receive");
    
    TS_START("updateMesh");
    updateMesh();
    TS_STOP("updateMesh");

    TS_START("updateSquash:Send");
    if( mSendMesh.get()){ sender.send(mesh); }
    TS_STOP("updateSquash:Send");

}

void ofApp::updateSquash()
{
    for(int i = 0 ; i < NUM_BUDDIES ; i++)
    {
        this->receivers[i].update();
        if (this->receivers[i].isFrameNew()) {
            this->receivers[i].receive(this->receivedMeshs[i]);
            mReceivedMeshNum = this->receivedMeshs[i].getNumVertices();
        }
    }
}

void ofApp::updateKinect()
{
    ofVec3f humanBox_center = mKinectSpaceRangesCenter.get();
    
    float _h = 200;
    float _w = 100;
    float _d = 70;
    
    
    if(mKinectEnable)
    {
        ///Kinect Enabl
        TS_START("kinect update");
        kinect0.update();
        TS_STOP("kinect update");

        TS_START("ReadFrom kinect");

        if (kinect0.isFrameNew()) {
            ofPixels pix = kinect0.getColorPixelsRef();
            
            int minY = mMinRangeDepth.get().y;
            int minX = mMinRangeDepth.get().x;
            
            int maxY = mMaxRangeDepth.get().y;
            int maxX = mMaxRangeDepth.get().x;
            
            mesh.clear();
            {
                int index = 0;
                float _mFilter_ZTDif_Thresh = mFilter_ZTDif_Thresh_Time.get();
                float _mFilter_ZTDif_Thresh_SP = mFilter_ZTDif_Thresh_Space.get();
            
                int _pass = 1;
                int _mSoundBufferDivRes = (int)mSoundBufferDivRes.get();
                
                //sound sequence
                int soundBufferSize = left.size();
                float t = (ofGetElapsedTimef());
                int step = mKinectReadResolusion.get();
                int h = kinect0.getDepthPixelsRef().getHeight();
                int w = kinect0.getDepthPixelsRef().getWidth();
                for(int y = 0; y < h; y += step) { if(y > minY && y < maxY){
                    for(int x = 0; x < w; x += step) { if(x > minX && x < maxX){
                        float dist = kinect0.getDistanceAt(x, y);
                        if(mMaxDepth > dist && mMinDepth < dist)
                        {
                            ofVec3f pt = kinect0.getWorldCoordinateAt(x, y, dist);
                            
                            //mZMapSmoothed[y][x] += (pt.z - mZMapSmoothed[y][x]) * 0.5;
                            
                            //HERE THIS IS FILTER PROCESS
                            //TEMPORAL FILTER
                            {
                                float dif_from_ex = pt.z - mZMap[y][x];
                                if(dif_from_ex > _mFilter_ZTDif_Thresh)
                                {
                                    _pass = -1;
                                    if(!mIsDoDebug){ continue; }
                                }else{
                                    _pass = 1;
                                }

                                //mZMap[y][x] = pt.z;
                            }
                            
                            //HERE THIS IS FILTER PROCESS
                            //SPATIAL FILTER
                            if(_pass == 1)
                            {
                                float dif_from_neib = pt.z - mZMap[y][(x-1)%w];
                                if(dif_from_neib / step > _mFilter_ZTDif_Thresh_SP)
                                {
                                    _pass = -2;
                                    if(!mIsDoDebug){ continue; }
                                }else{
                                    _pass = 1;
                                }
                            }
                            
                            
                            {
                                ofColor c;
                                float h = ofMap(dist, 50, 200, 0, 120, true);
//                                float r_x = pix.getWidth() / kinect0.getDepthPixelsRef().getWidth();
//                                float r_y = pix.getHeight() / kinect0.getDepthPixelsRef().getHeight();
//                                
                                //c = pix.getColor(x * r_x, y * r_y);
                                c.setHsb(h, 255, 255);
                                
                                mesh.addVertex(pt);
                                
                                //mesh.addVertex( ofVec3f(pt.x, pt.y, mZMapSmoothed[y][x]));
                                
                                if(_pass == 1){
                                    mesh.addColor(c);
                                }else if(_pass == -1){
                                    mesh.addColor(ofColor(0,0,255));
                                }else if(_pass == -2){
                                    mesh.addColor(ofColor(255,0,0));
                                }
                                
                                
                                mesh.addNormal(ofVec3f(
                                                       left[ (int)(x / _mSoundBufferDivRes * step) % soundBufferSize],
                                                       right[ (int)(y / _mSoundBufferDivRes * step) % soundBufferSize],
                                                       pt.z - mZMap[y][x]));
                                
//                                mesh.addNormal(ofVec3f(
//                                                       left[ (int)(x / _mSoundBufferDivRes * step) % soundBufferSize],
//                                                       right[ (int)(y / _mSoundBufferDivRes * step) % soundBufferSize],
//                                                       t));
                                
//                                mesh.addNormal(ofVec3f(
//                                                       left[ (int)(index / 50 * step) % soundBufferSize],
//                                                       right[ (int)(index / 50 * step) % soundBufferSize],
//                                                       t));
                                index ++;
                                mZMap[y][x] = pt.z;
                            }
                        }
                    }}
                }}
                
            }
        }
        mPointsNumber = mesh.getNumVertices();
        TS_STOP("ReadFrom kinect");

    }else{
        
        //Dummy data
        mesh = ofSpherePrimitive(100.0, 40).getMesh();
    }

}

void ofApp::updateMesh()
{
    //temporal
    testAnimation += (0.0 - testAnimation) * 0.1;
    
    return ;
    
    
    //test code
    //mesh = ofSpherePrimitive(100.0, 40).getMesh();
    auto & vertices = mesh.getVertices();
    
    ofVec3f cen = effectCenter.get();//ofVec3f(0,0,0);
    for (auto & vertex : vertices) {
        auto dif = (vertex - cen).length();
        auto theta = atan2f(vertex.z, vertex.x);
        auto thi = atan2f(vertex.y, vertex.x);
//        vertex += vertex * dif * testAnimation * 0.004 ;// * ofNoise(theta, thi, ofGetElapsedTimef());
        vertex = (vertex - cen) * testAnimation * effectAmp.get() + vertex;
        
        //ofNoise(theta, thi, ofGetElapsedTimef())
    }

    testAnimation += (0.0 - testAnimation) * 0.1;
    
//    if(ofGetKeyPressed())
//    {
//        testAnimation += (1.0 - testAnimation) * 0.8;
//    }else{
//        
//    }
    
    
    
}

//--------------------------------------------------------------
void ofApp::updateAudio(){
    //lets scale the vol up to a 0-1 range
    scaledVol = ofMap(smoothedVol, 0.0, 0.17, 0.0, 1.0, true);
    
    //lets record the volume into an array
    volHistory.push_back( scaledVol );
    
    //if we are bigger the the size we want to record - lets drop the oldest value
    if( volHistory.size() >= 400 ){
        volHistory.erase(volHistory.begin(), volHistory.begin()+1);
    }
}


//--------------------------------------------------------------
void ofApp::draw(){
    
    ofClear(50);
    
    TS_START("drawOVR");
    drawOVR();
    TS_STOP("drawOVR");

    
    ofEnableBlendMode(OF_BLENDMODE_ALPHA);
    
    TS_START("drawAudio");
    if(mIsDoDebug) { drawAudio(); }
    TS_STOP("drawAudio");
    
    ofSetupScreen();
    
    ofDisableDepthTest();
    ofEnableAlphaBlending();
    ofSetColor(255);

    TS_START("gui.draw");
    if(mIsDoDebug)
    {
        gui.draw();
        effect_gui.draw();
    }else{
        ofDrawBitmapString(ofToString(ofGetFrameRate()), 50, 50);
    }
    TS_STOP("gui.draw");
    
}

void ofApp::drawKinectSensors()
{
    
    
    ofPushMatrix();
    ofTranslate(500,500,0);
    ofScale(1.0, 1.0, 0.001);
    mesh.drawVertices();
    ofPopMatrix();

    
    
    

}

void ofApp::drawOVR()
{
    
    if(mDK2Enable)
    {
        
        if(oculusRift.isSetup()){
            
            ecam.begin();
            ecam.end();
            
            // - - - -- - - - - - - LEFT EYE - - - - - - - //
            TS_START("oculusRift rendering LEFT");
            ofSetColor(255);
            oculusRift.beginLeftEye();
            {
                ofPushStyle();
                ofNoFill();
                ofSetColor(255);
                ofDrawBox(100.0);
                ofPopStyle();
                
                drawScene();
            }
            oculusRift.endLeftEye();
            TS_STOP("oculusRift rendering LEFT");

            
            // - - - -- - - - - - - RIGHT EYE EYE - - - - - - - //
            TS_START("oculusRift rendering RIGTH");
            oculusRift.beginRightEye();
            {
                ofPushStyle();
                ofNoFill();
                ofSetColor(255);
                ofDrawBox(100.0);
                ofPopStyle();
                
                drawScene();
            }
            oculusRift.endRightEye();
            TS_STOP("oculusRift rendering RIGTH");
            
            TS_START("oculusRift rendering Screen");
            oculusRift.draw();
            TS_STOP("oculusRift rendering Screen");
        }
    }else{
        
        TS_START("Globalview rendering Screen");
        ofEasyCam *pcam = mOutSideView ? &gcam : &ecam;
        pcam->begin();
        
        {
            ofPushStyle();
            ofNoFill();
            ofSetColor(255);
            ofDrawBox(100.0);
            ofPopStyle();
            
            drawScene();
        }
        pcam->end();
        TS_STOP("Globalview rendering Screen");

    }
}

void ofApp::drawScene()
{
    bool _isIndication = true;
    
    TS_START("drawScene");
    
    ofPushStyle();
    ofPushMatrix();
    
    ofScale(0.01, 0.01, 0.01);

    if(_isIndication){
        ofDrawAxis(5.0f);
        ofDrawBitmapString("World Sensor 0", 0, 0);

        ofNoFill();
        ofDrawBox(0, 0, 0, 1);
        ofDrawBox(0, 0, 0, 10);
    }
    
    //if(mVirtuaFloor.get())
    {
        ofxGridDraw::drawGridFloor(ofPoint(0,mVirtuaFlooroffset, 0), ofVec3f(1000), ofVec3f(50));
    }
    
    ofTranslate(mKinectSpaceCalibTrans);
    ofRotate(mKinectSpaceCalibRot.get().x, 1.0, 0.0, 0.0);
    ofRotate(mKinectSpaceCalibRot.get().y, 0.0, 1.0, 0.0);
    ofRotate(mKinectSpaceCalibRot.get().z, 0.0, 0.0, 1.0);
    
    
    {
        
        //drawing kinect sensors
        if(_isIndication)
        {
            TS_START("Draw Indication");

            ofPushStyle();
            
            ofNoFill();
            ofDrawBox(0, 0, 0, 5);
            ofDrawBitmapString("Kinect Sensor", 0, 0);
            
            //ofDrawAxis(500.f);
            
            ofLine(0, 0, 0, 0, 0, 5000);
            for(int i = 0 ; i < 20 ; i++){
                
                ofPushMatrix();
                
                float size = 10;
                float cm = i * 25;
                ofTranslate(0, 0, cm);
                ofNoFill();
                ofRect(-size, -size, 2*size, 2*size);
                ofFill();
                ofSetColor(255, 255, 255, 50);
                ofRect(-size, -size, 2*size, 2*size);
                ofSetColor(255);
                ofDrawBitmapString(ofToString(cm) + "cm", 0,0,0);
                ofPopMatrix();
            }
            ofPopStyle();
    
            
            ofPushMatrix();
            ofTranslate(effectCenter);
            ofDrawAxis(10.0);
            ofDrawBitmapString("effectCenter", 0, 0, 0);
            ofPopMatrix();

            
            ofPushMatrix();
            ofTranslate(mKinectSpaceRangesCenter);
            ofDrawAxis(10.0);
            ofDrawBitmapString("KinectSpaceRanges", 0, 0, 0);
            ofPopMatrix();
            //humanBox_center
            
            TS_STOP("Draw Indication");
 
        }
        
        //ofEnableAntiAliasing();
        //ofEnableSmoothing();
        //glPointSize(4.0);
        drawVertexes();
        
        if(mVirtualMirror)
        {
            TS_START("VirtualMirror rendering");

            ofPushStyle();
            ofPushMatrix();
                ofTranslate(0, 0, virtualMirroroffset / 2.0);
                
                ofFill();
                ofSetColor(255, 255, 255, 50);
                ofRect(-500, mVirtuaFlooroffset, 1000, 500);
                
                ofNoFill();
                ofSetLineWidth(5.0);
                ofSetColor(0, 105, 255, 120);
                ofRect(-500, mVirtuaFlooroffset, 1000, 500);
                
            ofPopMatrix();
            ofPopStyle();
            
            //mirror itself
            ofPushMatrix();
                ofTranslate(0, 0, virtualMirroroffset);
                ofScale(1.0, 1.0, -1.0);
                {
                    ofEnableSmoothing();
                    ofEnableAntiAliasing();
                    glPointSize(2.0);
                    drawVertexes();
                }
            ofPopMatrix();
            TS_STOP("VirtualMirror rendering");

        }
    }
    
    ofPopMatrix();
    ofPopStyle();
    
    TS_STOP("drawScene");
}

void ofApp::drawVertexes()
{
    TS_START("drawVertexes rendering");
    //
    bool doShader = mDoShader.get();
    if(doShader) {
        ofEnablePointSprites();
        mShdInstanced->begin();
        
        mShdInstanced->setUniform1f("animationParameter0", testAnimation);
        mShdInstanced->setUniform1f("pointSizeAmp", (float)mKinectReadResolusion.get());
        mShdInstanced->setUniform3f("AmpCenterPos", effectCenter.get().x, effectCenter.get().y, effectCenter.get().z);
        
        textureImg.getTexture().bind();
        ofEnableBlendMode(OF_BLENDMODE_ADD);
    }
    
    //Normal
    {
        
        mesh.drawVertices();
        
        if(mReceivedMeshDraw.get())
        {
            for(auto receivedMesh : receivedMeshs)
            {
                ofPushMatrix();
                ofTranslate(mReceiveMeshOffset.get());
                //ofTranslate(50, 0);
                receivedMesh.drawVertices();
                ofPopMatrix();
            }
        }
    }
    
    if(copyHuman.get() > 0)
    {
        for (int i = 1 ; i < copyHuman.get() ; i ++)
        {
            //copyHuman
            ofPushMatrix();
            ofTranslate(i * copyHumanIntervel.get());
            mesh.drawVertices();
            ofPopMatrix();

            ofPushMatrix();
            ofTranslate(i * -1.0 * copyHumanIntervel.get());
            mesh.drawVertices();
            ofPopMatrix();
        
        }
        
    }
    
    
    if(doShader){
        textureImg.getTexture().unbind();
        ofDisablePointSprites();
        mShdInstanced->end();
    }
    TS_STOP("drawVertexes rendering");
    
}

void ofApp::drawAudio(){
    ofSetupScreen();
    ofSetColor(0,0,0,120);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    ofSetColor(225);
    ofNoFill();
    
    // draw the left channel:
    ofPushStyle();
    ofPushMatrix();
    ofTranslate(32, 170, 0);
    
    ofSetColor(225);
    ofDrawBitmapString("Left Channel", 4, 18);
    
    ofSetLineWidth(1);
    ofDrawRectangle(0, 0, 512, 200);
    
    ofSetColor(245, 58, 135);
    ofSetLineWidth(3);
    
    ofBeginShape();
    for (unsigned int i = 0; i < left.size(); i++){
        ofVertex(i*2, 100 -left[i]*180.0f);
    }
    ofEndShape(false);
    
    ofPopMatrix();
    ofPopStyle();
    
    // draw the right channel:
    ofPushStyle();
    ofPushMatrix();
    ofTranslate(32, 370, 0);
    
    ofSetColor(225);
    ofDrawBitmapString("Right Channel", 4, 18);
    
    ofSetLineWidth(1);
    ofDrawRectangle(0, 0, 512, 200);
    
    ofSetColor(245, 58, 135);
    ofSetLineWidth(3);
    
    ofBeginShape();
    for (unsigned int i = 0; i < right.size(); i++){
        ofVertex(i*2, 100 -right[i]*180.0f);
    }
    ofEndShape(false);
    
    ofPopMatrix();
    ofPopStyle();
    
    // draw the average volume:
    ofPushStyle();
    ofPushMatrix();
    ofTranslate(565, 170, 0);
    
    ofSetColor(225);
    ofDrawBitmapString("Scaled average vol (0-100): " + ofToString(scaledVol * 100.0, 0), 4, 18);
    ofDrawRectangle(0, 0, 400, 400);
    
    ofSetColor(245, 58, 135);
    ofFill();
    ofDrawCircle(200, 200, scaledVol * 190.0f);
    
    //lets draw the volume history as a graph
    ofBeginShape();
    for (unsigned int i = 0; i < volHistory.size(); i++){
        if( i == 0 ) ofVertex(i, 400);
        
        ofVertex(i, 400 - volHistory[i] * 70);
        
        if( i == volHistory.size() -1 ) ofVertex(i, 400);
    }
    ofEndShape(false);
    
    ofPopMatrix();
    ofPopStyle();
    
    drawCounter++;
    
    ofSetColor(225);
    string reportString = "buffers received: "+ofToString(bufferCounter)+"\ndraw routines called: "+ofToString(drawCounter)+"\nticks: " + ofToString(soundStream.getTickCount());
    ofDrawBitmapString(reportString, 32, 589);
    
}


//--------------------------------------------------------------

void ofApp::onReceiveOSC(ofxOscMessage & prt)
{
    static int lastHit = 0;//ofGetFrameNum();
    
    if(prt.getArgAsInt(0) == hitFinderChannel.get())
    {
        //
        if(ofGetFrameNum() - lastHit > 30 )
        {
            testAnimation = 1.0;
            cout << "Hit frame" << endl;
            lastHit = ofGetFrameNum();
        }
        
        mSignalBang.set(testAnimation);
    }
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if(key == 'f'){
        ofToggleFullscreen();
            }
    else if(key == 's'){
        gui.saveToFile("mySettings.xml");
        effect_gui.saveToFile("mySettingsEffect.xml");

    }else if(key == 'c'){
        oculusRift.reset();
    }else if(key == 'o'){
        mDK2Enable = !mDK2Enable;
    }else if(key == 'd'){
        mIsDoDebug = !mIsDoDebug;
        mDoDebug.set(mIsDoDebug);
    }else if(key == 'm'){
        mVirtualMirror.set(!mVirtualMirror.get());
    }else if(key == 'g'){
        mVirtuaFloor.set(!mVirtuaFloor.get());
    }else if(key == OF_KEY_RIGHT){
        copyHuman.set(copyHuman.get() + 1);
    }else if(key == OF_KEY_LEFT){
        copyHuman.set(copyHuman.get() - 1);
    }
    
    
    if(key == ' '){
        testAnimation = 1.0;
    }
    
    
    if(key == '1'){
        vertShaderFileName = "Shaders_GL3/simpleAmp.vert";
        fragShaderFileName = "Shaders_GL3/simpleAmp.frag";

    }else if(key == '2'){
        
        vertShaderFileName = "Shaders_GL3/simplePosAmp.vert";
        fragShaderFileName = "Shaders_GL3/simpleAmp.frag";

    }
    

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------


void ofApp::audioIn(float * input, int bufferSize, int nChannels){
    
    float curVol = 0.0;
    // samples are "interleaved"
    int numCounted = 0;
    
    //lets go through each sample and calculate the root mean square which is a rough way to calculate volume
    for (int i = 0; i < bufferSize; i++){
        left[i]		= input[i*2]*0.5;
        right[i]	= input[i*2+1]*0.5;
        
        curVol += left[i] * left[i];
        curVol += right[i] * right[i];
        numCounted+=2;
    }
    
    //this is how we get the mean of rms :)
    curVol /= (float)numCounted;
    
    // this is how we get the root of rms :)
    curVol = sqrt( curVol );
    
    smoothedVol *= 0.93;
    smoothedVol += 0.07 * curVol;
    
    bufferCounter++;
    
}


