#version 330

uniform mat4 modelViewProjectionMatrix; // automatically imported by OF
uniform mat4 modelViewMatrix; // automatically imported by OF
uniform mat4 normalMatrix; // the normal matrix (the inversed-then-transposed modelView matrix)
uniform mat4 textureMatrix;
uniform mat4 projectionMatrix;

uniform float pointSizeAmp;
uniform float animationParameter0;
uniform vec3 AmpCenterPos;


in vec4 position; // in local space
in vec3 normal; // in local space
in vec4 color;

out vec4 eyeSpaceVertexPos, ambientGlobal;
out vec3 vertex_normal, interp_eyePos;
out vec4 colorV;

vec4 pos;

void main() {
  // ambientGlobal = material.emission; // no global lighting for the moment
  // eyeSpaceVertexPos = modelViewMatrix * position;
  // vertex_normal = normalize((normalMatrix * vec4(normal, 0.0)).xyz);
  // interp_eyePos = vec3(-eyeSpaceVertexPos);
    
    pos = position;
    //This is part to change position
    pos.xyz = position.xyz * 1.0;
    
    vec4 eyeCoord = modelViewMatrix * pos;
    float dist = sqrt(eyeCoord.x*eyeCoord.x + eyeCoord.y*eyeCoord.y + eyeCoord.z*eyeCoord.z);
    float att	 = 3.0 / dist * pointSizeAmp;
    
//    vec4 soundwave = vec4(normal.z * 0.01, 0.0, normal.z * 0.01, 0.0);
    float amp = 1.0;
    vec4 soundwave = vec4(normal.x * amp, normal.y * amp, 0.0, 0.0);
    
    gl_Position = modelViewProjectionMatrix * pos + soundwave;// normal.x * 0.3 + normal.y * 0.3;
    gl_PointSize = att * (1.0 + normal.x * 20);//10000.0;//normal.x * att;

    colorV = color;
}
