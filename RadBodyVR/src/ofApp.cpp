#include "ofMain.h"
#include "ofxMultiKinectV2.h"
#include "ofxOculusDK2.h"


#define STRINGIFY(x) #x

static string depthFragmentShader =
STRINGIFY(
          uniform sampler2DRect tex;
          void main()
          {
              vec4 col = texture2DRect(tex, gl_TexCoord[0].xy);
              float value = col.r;
              float low1 = 500.0;
              float high1 = 5000.0;
              float low2 = 1.0;
              float high2 = 0.0;
              float d = clamp(low2 + (value - low1) * (high2 - low2) / (high1 - low1), 0.0, 1.0);
              if (d == 1.0) {
                  d = 0.0;
              }
              gl_FragColor = vec4(vec3(d), 1.0);
          }
          );

static string irFragmentShader =
STRINGIFY(
          uniform sampler2DRect tex;
          void main()
          {
              vec4 col = texture2DRect(tex, gl_TexCoord[0].xy);
              float value = col.r / 65535.0;
              gl_FragColor = vec4(vec3(value), 1.0);
          }
          );

#include "GpuRegistration.h"


//========================================================================
class ofApp : public ofBaseApp{
    
    
    ofxOculusDK2		oculusRift;

    
    ofShader depthShader;
    ofShader irShader;
    
    
    ofxMultiKinectV2 kinect0;
    ofTexture colorTex0;
    ofTexture depthTex0;
    ofTexture irTex0;
    
    GpuRegistration gr;

    bool process_occlusion;
    
    ofEasyCam ecam;
    ofVboMesh mesh;
public:
    
    void setup()
    {
        process_occlusion = false;
        
        ofSetVerticalSync(true);
        ofSetFrameRate(75);
        
        depthShader.setupShaderFromSource(GL_FRAGMENT_SHADER, depthFragmentShader);
        depthShader.linkProgram();
        
        irShader.setupShaderFromSource(GL_FRAGMENT_SHADER, irFragmentShader);
        irShader.linkProgram();
        
        kinect0.open(true, true, 0, 2);
        // Note :
        // Default OpenCL device might not be optimal.
        // e.g. Intel HD Graphics will be chosen instead of GeForce.
        // To avoid it, specify OpenCL device index manually like following.
        // kinect1.open(true, true, 0, 2); // GeForce on MacBookPro Retina
        
        kinect0.start();
        
        mesh.setUsage(GL_DYNAMIC_DRAW);
        mesh.setMode(OF_PRIMITIVE_POINTS);
        
        ecam.setAutoDistance(false);
        ecam.begin();
        ecam.end();
        //ecam.setGlobalPosition(0, 1.9, 3);
        
        ecam.setDistance(200);
        
        oculusRift.baseCamera = &ecam;
        oculusRift.setup();
        oculusRift.setUsePredictedOrientation(true);
        oculusRift.lockView = false;
        oculusRift.reloadShader();
        
    }
    
    void update() {
        
        
        kinect0.update();
        if (kinect0.isFrameNew()) {
//
            colorTex0.loadData(kinect0.getColorPixelsRef());
//
            depthTex0.loadData(kinect0.getDepthPixelsRef());
//            irTex0.loadData(kinect0.getIrPixelsRef());
//            
//            depthTex0.setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);
//            gr.update(depthTex0, colorTex0, process_occlusion);
            
            ofPixels pix = kinect0.getColorPixelsRef();
            
            mesh.clear();
            {
                int step = 2;
                int h = kinect0.getDepthPixelsRef().getHeight();
                int w = kinect0.getDepthPixelsRef().getWidth();
                for(int y = 0; y < h; y += step) {
                    for(int x = 0; x < w; x += step) {
                        float dist = kinect0.getDistanceAt(x, y);
                        if(dist > 10 && dist < 100) {
                            ofVec3f pt = kinect0.getWorldCoordinateAt(x, y, dist);
                            
                            ofColor c;
                            float h = ofMap(dist, 50, 200, 0, 255, true);
                            
                            //test
                            float r_x = pix.getWidth() / kinect0.getDepthPixelsRef().getWidth();
                            float r_y = pix.getHeight() / kinect0.getDepthPixelsRef().getHeight();
                            
                            //c = pix.getColor(x * r_x, y * r_y);

                            c.setHsb(h, 255, 255);
                            mesh.addColor(c);
                            mesh.addVertex(pt);
                        }
                    }
                }

            }
        }
    }
    
    void drawOVR()
    {
        float scaleSize = 3000;
        if(oculusRift.isSetup()){
            
            ecam.begin();
            ecam.end();
            
            
            ofSetColor(255);
            
            // - - - -- - - - - - - LEFT EYE - - - - - - - //
            ofSetColor(255);
            oculusRift.beginLeftEye();
            //drawOVREye();
            
            ofPushStyle();
            ofNoFill();
            ofSetColor(255);
            ofDrawBox(100.0);
            ofPopStyle();
            
            if (depthTex0.isAllocated()) {
                ofPushStyle();
                glPointSize(2);
                //ecam.begin();
                ofDrawAxis(100);
                ofPushMatrix();
                ofTranslate(0, 0, -100);
                mesh.draw();
                ofPopMatrix();
                //ecam.end();
                ofPopStyle();
            }
            
            
            oculusRift.endLeftEye();
            
            
            // - - - -- - - - - - - RIGHT EYE EYE - - - - - - - //
            oculusRift.beginRightEye();
            //drawOVREye();
            
            ofPushStyle();
            ofNoFill();
            ofSetColor(255);
            ofDrawBox(100.0);
            ofPopStyle();
            
            if (depthTex0.isAllocated()) {
                ofPushStyle();
                glPointSize(2);
                //ecam.begin();
                ofDrawAxis(100);
                ofPushMatrix();
                ofTranslate(0, 0, -100);
                mesh.draw();
                ofPopMatrix();
                //ecam.end();
                ofPopStyle();
            }
            
            
            oculusRift.endRightEye();
            
            
            oculusRift.draw();
            
            
        }
    }
    
    
    
    void draw()
    {
        
        
        ofClear(50);
        
        /*
        
        if (colorTex0.isAllocated()) {
            colorTex0.draw(0, 0, 640, 360);
        }
        
        if (kinect0.isFrameNew()) {
            
        if (ofGetKeyPressed(' ')) {
            gr.getRegisteredTexture(process_occlusion).draw(640, 0, 1024, 848);
        } else {
            
            irShader.begin();
            irTex0.draw(640, 0, 1024, 848);
            irShader.end();
            
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255);
            //gr.getRegisteredTexture(process_occlusion).draw(640, 0, 1024, 848);
            ofPopStyle();
        }
        }*/
        
        
        if (depthTex0.isAllocated()) {
            ofPushStyle();
            glPointSize(2);
            ecam.begin();
            ofDrawAxis(100);
            ofPushMatrix();
            ofTranslate(0, 0, -100);
            mesh.draw();
            ofPopMatrix();
            ecam.end();
            ofPopStyle();
        }
        
        drawOVR();
    
        
        ofDrawBitmapStringHighlight(ofToString(ofGetFrameRate()), 10, 20);
        ofDrawBitmapStringHighlight("Device Count : " + ofToString(ofxMultiKinectV2::getDeviceCount()), 10, 40);
    }

    void keyPressed(int key)
    {
        if (key == 'd') {
            process_occlusion =  !process_occlusion;
        }
        
        if (key == 'f') {
            ofToggleFullscreen();
        }
        
    }
    
    
};

//#include "ofAppGLFWWindow.h"
//========================================================================
int main( ){
    ofGLWindowSettings settings;
    settings.width = 1920;
    settings.height = 1080;
    settings.setGLVersion(4,0); /// < select your GL Version here
    ofCreateWindow(settings); ///< create your window here
    
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(new ofApp());
    
}
