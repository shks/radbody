#include "ofMain.h"
#include "ofxMultiKinectV2.h"

#define STRINGIFY(x) #x

static string depthFragmentShader =
STRINGIFY(
          uniform sampler2DRect tex;
          void main()
          {
              vec4 col = texture2DRect(tex, gl_TexCoord[0].xy);
              float value = col.r;
              float low1 = 500.0;
              float high1 = 5000.0;
              float low2 = 1.0;
              float high2 = 0.0;
              float d = clamp(low2 + (value - low1) * (high2 - low2) / (high1 - low1), 0.0, 1.0);
              if (d == 1.0) {
                  d = 0.0;
              }
              gl_FragColor = vec4(vec3(d), 1.0);
          }
          );

static string irFragmentShader =
STRINGIFY(
          uniform sampler2DRect tex;
          void main()
          {
              vec4 col = texture2DRect(tex, gl_TexCoord[0].xy);
              float value = col.r / 65535.0;
              gl_FragColor = vec4(vec3(value), 1.0);
          }
          );

#include "GpuRegistration.h"

//========================================================================
class ofApp : public ofBaseApp{
//    ofShader depthShader;
//    ofShader irShader;
    
//    ofxMultiKinectV2 kinect0;
//    ofTexture colorTex0;
//    ofTexture depthTex0;
//    ofTexture irTex0;
//    GpuRegistration gr0;
//
//    
//    ofxMultiKinectV2 kinect1;
//    ofTexture colorTex1;
//    ofTexture depthTex1;
//    ofTexture irTex1;
//    GpuRegistration gr1;
    
    
    ofxMultiKinectV2 kinect[2];
    ofTexture colorTex[2];
    ofTexture depthTex[2];
    ofTexture irTex[2];
    GpuRegistration gr[2];
    ofPixels pix[2];

    
    bool process_occlusion;
    
    ofEasyCam ecam[2];
    ofVboMesh mesh[2];
    
public:
    
    void setup()
    {
        process_occlusion = false;
        
        ofSetVerticalSync(true);
        ofSetFrameRate(60);
        
//        depthShader.setupShaderFromSource(GL_FRAGMENT_SHADER, depthFragmentShader);
//        depthShader.linkProgram();
//        
//        irShader.setupShaderFromSource(GL_FRAGMENT_SHADER, irFragmentShader);
//        irShader.linkProgram();
        
        kinect[0].open(true, true, 0, 2);
        kinect[1].open(true, true, 1, 2);
        // Note :
        // Default OpenCL device might not be optimal.
        // e.g. Intel HD Graphics will be chosen instead of GeForce.
        // To avoid it, specify OpenCL device index manually like following.
        // kinect1.open(true, true, 0, 2); // GeForce on MacBookPro Retina
        
        kinect[0].start();
        kinect[1].start();
        
        gr[0].setup(kinect[0].getProtonect(), 2);
        gr[1].setup(kinect[1].getProtonect(), 2);
        
        
        mesh[0].setUsage(GL_DYNAMIC_DRAW);
        mesh[0].setMode(OF_PRIMITIVE_POINTS);
        
        ecam[0].setAutoDistance(false);
        ecam[0].setDistance(200);
        
        
        mesh[1].setUsage(GL_DYNAMIC_DRAW);
        mesh[1].setMode(OF_PRIMITIVE_POINTS);
        
        ecam[1].setAutoDistance(false);
        ecam[1].setDistance(200);
        
        
    }
    
    void update() {
        updateKinectData(0);
        updateKinectData(1);
    }
    
    void updateKinectData(int i)
    {
        kinect[i].update();
        if (kinect[i].isFrameNew()) {
            colorTex[i].loadData(kinect[i].getColorPixelsRef());
            depthTex[i].loadData(kinect[i].getDepthPixelsRef());
            irTex[i].loadData(kinect[i].getIrPixelsRef());
            depthTex[i].setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);
            gr[i].update(depthTex[i], colorTex[i], process_occlusion);
            
            gr[i].getRegisteredTexture(false).readToPixels(pix[i]);
            
            mesh[i].clear();
            {
                int step = 1;
                int h = kinect[i].getDepthPixelsRef().getHeight();
                int w = kinect[i].getDepthPixelsRef().getWidth();
                for(int y = 0; y < h; y += step) {
                    for(int x = 0; x < w; x += step) {
                        float dist = kinect[i].getDistanceAt(x, y);
                        if(dist > 50 && dist < 500) {
                            //waht ?¥
                            ofVec3f pt = kinect[i].getWorldCoordinateAt(x, y, dist);
                            
                            ofColor c;
                            float h = ofMap(dist, 50, 200, 0, 255, true);
                            
                            //test
                            float r_x = pix[i].getWidth() / kinect[i].getDepthPixelsRef().getWidth();
                            float r_y = pix[i].getHeight() / kinect[i].getDepthPixelsRef().getHeight();
                            
                            c = pix[i].getColor(x * r_x, y * r_y);
                            
                            //c.setHsb(h, 255, 255);
                            mesh[i].addColor(c);
                            mesh[i].addVertex(pt);
                        }
                    }
                }
                
            }
        }
    }
    

    void draw()
    {
        ofClear(0);
        //drawKinectData(1);
        
        ofEnableAlphaBlending();
        
        
        for(int i = 0 ; i < 1 ; i++)
        {
            if(pix[i].isAllocated() ){
                ofImage img;
                img.setFromPixels(pix[i]);
                
                
                {
                    ofPushMatrix();
                    ofTranslate(50, 50);
                    ofSetColor(12, 255, 120);
                    ofDrawBitmapStringHighlight("RegisteredTexture: " + ofToString(img.getWidth()) + "x" + ofToString(img.getHeight()), 0, 0);
                    ofSetColor(255);
                    img.draw(0, 0, img.getWidth() * 0.5, img.getHeight() * 0.5);
                    ofPopMatrix();
                }

                {
                    ofPushMatrix();
                    ofTranslate(640, 50);
                    ofSetColor(12, 255, 120);
                    ofDrawBitmapStringHighlight("colorTex: " + ofToString(colorTex[i].getWidth()) + "x" + ofToString(colorTex[i].getHeight()), 0, 0);
                    ofSetColor(255);
                    colorTex[i].draw(0, 0, colorTex[i].getWidth() * 0.5,  colorTex[i].getHeight() * 0.5 );
                    ofPopMatrix();
                }

                {
                    ofPushMatrix();
                    ofTranslate(0, 500);
                    ofSetColor(12, 255, 120);
//                    ofDrawBitmapStringHighlight("depthTex: " + ofToString(depthTex[i].getWidth()) + "x" + ofToString(depthTex[i].getHeight()) + "glTypeInternal : " + ofToString( depthTex[i].getTextureData().glTypeInternal ), 0, 0);
                    ofSetColor(255);
                    
                    ofImage depthImg;
                    ofFloatImage fImag;
                    ofShortImage shortDepthImg;
                    
                    
                    
                    shortDepthImg.setFromPixels(kinect[i].getDepthPixelsRef());
                    depthImg.setFromPixels(kinect[i].getDepthPixelsRef() );
                    fImag.setFromPixels(kinect[i].getDepthPixelsRef());
                    
 
 //                   depthImg.draw(50,0);
                    fImag.draw(50, 0);
                    shortDepthImg.draw(50, 500);
                    
                    
                    if(ofGetKeyPressed())
                    {
                        for(int x = 0 ; x < fImag.getWidth() ; x++)
                        {
                            
                            for(int y = 0 ; y < fImag.getHeight() ; y++)
                            {
                                ofFloatColor col = fImag.getColor(x, y);
                                cout << col << endl;
                                
                                
                                ofFloatColor short_col = shortDepthImg.getColor(x, y);
                                cout << short_col << endl;
                            }
                            
                        }
                    }
                    
                    
                    //depthTex[i].draw(0, 0, depthTex[i].getWidth() * 1.0,  depthTex[i].getHeight() * 1.0 );
//                    depthTex[i].getTextureData().glTypeInternal;
                    ofPopMatrix();
                }
                
            }
            
        }

        //drawKinectData(0);

    }
    
    void drawKinectData(int i)
    {
        if (colorTex[i].isAllocated()) {
            colorTex[i].draw(0, 360 * i, 640, 360);
        }
        if (depthTex[i].isAllocated()) {
            
            if (ofGetKeyPressed(' ')) {
                ofPushStyle();
                ofEnableAlphaBlending();
                ofSetColor(255);
                gr[i].getRegisteredTexture(process_occlusion).draw(640, 0, 1024, 848);
                ofPopStyle();
            }
            
//            depthShader.begin();
//            depthTex0.draw(0, 424, 512, 424);
//            depthShader.end();
        }
        
        if (depthTex[i].isAllocated()) {
            ofPushStyle();
            glPointSize(2);
            ecam[i].begin();
            ofDrawAxis(100);
            ofPushMatrix();
            ofTranslate(0, 0, -100);
            mesh[i].draw();
            ofPopMatrix();
            ecam[i].end();
            ofPopStyle();
        }
        
        ofDrawBitmapStringHighlight(ofToString(ofGetFrameRate()), 10, 20);
        ofDrawBitmapStringHighlight("Device Count : " + ofToString(ofxMultiKinectV2::getDeviceCount()), 10, 40);
        if (process_occlusion) {
            ofDrawBitmapStringHighlight("Process Occlusion Mask Enable", 10, 60);
        }
        ofDrawBitmapStringHighlight("Mesh vertex number : " + ofToString(mesh[0].getNumVertices()), 10, 80);
        
    }

    void keyPressed(int key)
    {
        if (key == 'd') {
            process_occlusion =  !process_occlusion;
        }
    }
};

//#include "ofAppGLFWWindow.h"
//========================================================================
int main( ){
    ofSetupOpenGL(1920,1080,OF_WINDOW);            // <-------- setup the GL context
    
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(new ofApp());
    
}
